from flask import Flask, request, jsonify

app = Flask(__name__)

todos = {}
todo_id_counter = 0

class Todo:
    def __init__(self, title, description):
        self.title = title
        self.description = description

@app.route('/api/todo/create', methods=['POST'])
def create_todo():
    data = request.get_json()
    title = data.get('title')
    description = data.get('description')
    todo = Todo(title, description)
    global todo_id_counter
    todos[todo_id_counter] = todo
    todo_id_counter += 1
    return jsonify({'title': todo.title, 'description': todo.description}), 201

@app.route('/api/todo/update/<int:todo_id>', methods=['PUT'])
def update_todo(todo_id):
    data = request.get_json()
    title = data.get('title')
    description = data.get('description')
    if todo_id in todos:
        todo = todos[todo_id]
        todo.title = title
        todo.description = description
        return jsonify({'title': todo.title, 'description': todo.description}), 200
    else:
        return jsonify({'error': 'Todo not found'}), 404

@app.route('/api/todo/delete/<int:todo_id>', methods=['DELETE'])
def delete_todo(todo_id):
    if todo_id in todos:
        del todos[todo_id]
        return jsonify({'message': 'Todo deleted'}), 200
    else:
        return jsonify({'error': 'Todo not found'}), 404

@app.route('/api/todo/<int:todo_id>', methods=['GET'])
def get_todo(todo_id):
    if todo_id in todos:
        todo = todos[todo_id]
        return jsonify({'title': todo.title, 'description': todo.description}), 200
    else:
        return jsonify({'error': 'Todo not found'}), 404

if __name__ == '__main__':
    app.run()