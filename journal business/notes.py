from flask import Flask, request, jsonify

app = Flask(__name__)

notes = []

class Note:
    def __init__(self, title, content):
        self.title = title
        self.content = content

@app.route('/api/notes/create', methods=['POST'])
def create_note():
    data = request.get_json()
    title = data.get('title')
    content = data.get('content')
    
    if not title or not content:
        return jsonify({'error': 'Title and content are required'}), 400
    
    note = Note(title, content)
    notes.append(note)
    return jsonify({'title': note.title, 'content': note.content}), 201

@app.route('/api/notes/update/<int:note_id>', methods=['PUT'])
def update_note(note_id):
    data = request.get_json()
    title = data.get('title')
    content = data.get('content')
    
    if not title or not content:
        return jsonify({'error': 'Title and content are required'}), 400
    
    if note_id >= len(notes):
        return jsonify({'error': 'Note not found'}), 404
    
    note = notes[note_id]
    note.title = title
    note.content = content
    return jsonify({'title': note.title, 'content': note.content}), 200

@app.route('/api/notes/delete/<int:note_id>', methods=['DELETE'])
def delete_note(note_id):
    if note_id >= len(notes):
        return jsonify({'error': 'Note not found'}), 404
    
    note = notes[note_id]
    notes.remove(note)
    return jsonify({'message': 'Note deleted'}), 200

@app.route('/api/notes/<int:note_id>', methods=['GET'])
def get_note(note_id):
    if note_id >= len(notes):
        return jsonify({'error': 'Note not found'}), 404
    
    note = notes[note_id]
    return jsonify({'title': note.title, 'content': note.content}), 200

if __name__ == '__main__':
    app.run()