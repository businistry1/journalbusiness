# API Documentation

## Todo Endpoints

### Create Todo
- Endpoint: /api/todo/create
- Method: POST
- Parameters: title (string), description (string)
- Response: JSON object with the created todo details

### Update Todo
- Endpoint: /api/todo/update/{todo_id}
- Method: PUT
- Parameters: todo_id (integer), title (string), description (string)
- Response: JSON object with the updated todo details

### Delete Todo
- Endpoint: /api/todo/delete/{todo_id}
- Method: DELETE
- Parameters: todo_id (integer)
- Response: JSON object with the success message

### Get Todo
- Endpoint: /api/todo/{todo_id}
- Method: GET
- Parameters: todo_id (integer)
- Response: JSON object with the todo details

## Notes Endpoints

### Create Note
- Endpoint: /api/notes/create
- Method: POST
- Parameters: title (string), content (string)
- Response: JSON object with the created note details

### Update Note
- Endpoint: /api/notes/update/{note_id}
- Method: PUT
- Parameters: note_id (integer), title (string), content (string)
- Response: JSON object with the updated note details

### Delete Note
- Endpoint: /api/notes/delete/{note_id}
- Method: DELETE
- Parameters: note_id (integer)
- Response: JSON object with the success message

### Get Note
- Endpoint: /api/notes/{note_id}
- Method: GET
- Parameters: note_id (integer)
- Response: JSON object with the note details
