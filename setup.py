from setuptools import setup

setup(
    name='journal_business',
    version='1.0',
    packages=['journal_business'],
    install_requires=[
        'flask',
    ],
)