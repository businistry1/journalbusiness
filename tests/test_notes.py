
import pytest
from notes import app

@pytest.fixture
def client():
    app.testing = True
    return app.test_client()

def test_startup(client):
    assert client  # Ensure the client is created successfully
